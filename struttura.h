#pragma once

#include <unistd.h>
#include "linked_list.h"


#define GET_FILM 1
#define POST_BOOKING 2
#define DESTROY_BOOKING 3
#define CLIENT_EXIT 4
#define BOOKING_OK 111
#define BOOKING_FAULT 888
#define DESTROY_COMPLET 222
#define DESTROY_FAULT 777
#define DIM 100
#define MAX_SEATS_IN_ROOM 36
#define SIZESTRING 5
#define NUMSALE 3

typedef struct {
	char fila; 
	int numero; 
} Posto; 

typedef struct { 
	ListItem biglietti; 
	int codice;
	Posto posto; 
} Prenotazione; 

typedef struct {
	char* film;						// nome film
	int max_posti; 					// max numero di posti
	int prenotazioni_attive;
	ListHead lista_prenotazioni;	
} Sala;

typedef struct {
  int num_sale;
  Sala* sala_array;
} Cinema;

void Sala_init(Sala* sala_,char* nome, int max, int prenotation);
void Sala_print(Sala* sala_);

void Cinema_init(Cinema* cinema_, int num_rooms_, char** room_names_);
void Cinema_print(Cinema* cinema_);
void Cinema_destroy(Cinema* cinema);

void Prenotazione_print(ListHead* head);
void Prenotazione_create(ListHead* list, Posto* lista_Posti, int size_prenotazione, int codice_prenotazione);
int Prenotazione_find(ListHead* list_, int codice_);
int Prenotazione_occupata(ListHead* head, int number, char fila);

ssize_t writeIntero(int ds, int* numero);
ssize_t readIntero(int ds, int* numero);
ssize_t writeListaPosti(int ds, Posto* lista, int numposti); 
ssize_t readListaPosti(int ds, Posto * lista, int numposti);

int isPostoInLista(Posto p, Posto* lista, int size);
int isPostoValido(Posto* p); 
int isNumeroValido(int numero); 
int isFilaValida(char *fila);
int Indice_valido(int indice);

void Posto_print(Posto* p, int size);
int Indice_valido(int indice);



