CC = gcc
LIBS = -lglut -lGLU -lGL -lpthread -lm
SOURCE = struttura.c linked_list.c 
SOURCE1 = struttura.c linked_list.c 

all: client_test server_test 

client_test: client.c
	$(CC) -std=gnu99 client.c $(SOURCE) -o client_test $(LIBS)

server_test : server.c 
	$(CC) -std=gnu99 server.c $(SOURCE1) -o server_test $(LIBS)

clean:
	rm -f *.o
	rm -f server_test
	rm -f client_test
	
