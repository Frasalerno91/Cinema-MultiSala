#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdlib.h> 
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "struttura.h"

ssize_t writeIntero(int ds, int*numero){
	
	uint32_t tmp; 
	tmp = htonl(*numero); 
	return write(ds, &tmp, sizeof(uint32_t));	
	
}
ssize_t readIntero(int ds, int *numero){
	
	uint32_t tmp; 
	ssize_t ret; 
	ret = read(ds, &tmp, sizeof(uint32_t));
	*numero = ntohl(tmp); 
	return ret; 
} 

ssize_t writeListaPosti(int ds, Posto* lista, int numposti)
{
	ssize_t tot=0;
	int i;
	for (i=0; i<numposti; i++)
	{
		tot=tot + write(ds, &lista[i].fila, 1);
		tot=tot + writeIntero(ds, &lista[i].numero);
	}
	return tot;
}

ssize_t readListaPosti(int ds, Posto * lista, int numposti)
{
	ssize_t tot=0;
	int i;
	for (i=0; i<numposti; i++)
	{
		tot=tot + read(ds, &lista[i].fila, 1);
		tot=tot + readIntero(ds, &lista[i].numero);
	}
	return tot;
}
	

void Sala_print(Sala* sala_) {
		printf("sala: %s\n", sala_->film);
		printf("posti disponibili: %d\n", sala_->max_posti);
		printf("numero prenotazioni:%d\n", sala_->prenotazioni_attive);
		printf("prenotazioni confermate:\n");
		Prenotazione_print(&sala_->lista_prenotazioni);
		printf("\n---------------\n");
}
void Sala_init(Sala* sala_,  char* nome, int max, int prenotation){
	List_init(&sala_->lista_prenotazioni);
	sala_->film = nome;
	sala_->max_posti = max; 			
	sala_->prenotazioni_attive = prenotation;
}

// metodi relativi allo cinema
void Cinema_init(Cinema* cinema_, int num_rooms_, char** room_names_) {
  cinema_->num_sale = num_rooms_;
  cinema_->sala_array = (Sala*)malloc(num_rooms_*sizeof(Sala));
  for (int i = 0; i < num_rooms_; ++i) {
    char* curr_room_name = room_names_[i];
    Sala* curr_room = &cinema_->sala_array[i];
    Sala_init(curr_room, curr_room_name, MAX_SEATS_IN_ROOM, 0);
  }
}

void Cinema_print(Cinema* cinema_) {
  for (int i = 0; i < cinema_->num_sale; ++i) {
    Sala_print(&cinema_->sala_array[i]);
  }
}

void Cinema_destroy(Cinema* cinema_){
		free(cinema_->sala_array); 
		return;
}

//metodi relativi alle Prenotazioni

void Prenotazione_print(ListHead* head){
  ListItem* aux=head->first;
  
  while(aux){
    printf("[");
    Prenotazione* element = (Prenotazione*) aux;
    printf("fila:%c numero:%d codicePrenotazione:%d", element->posto.fila, element->posto.numero, element->codice);
    aux=aux->next;
	printf("]\n");
  }

}

void Prenotazione_create(ListHead* list, Posto* posti,  int size_prenotazione, int codice_prenotazione){
			Prenotazione* pr; 
	for(int i = 0; i <size_prenotazione; ++i){
			pr = (Prenotazione*) malloc(sizeof(Prenotazione));
			pr->biglietti.next = 0;
			pr->biglietti.prev = 0;
			pr->codice =codice_prenotazione;
			pr->posto.fila = posti[i].fila;
			pr->posto.numero = posti[i].numero;
			List_insert(list, list->last,(ListItem*)pr);	
	}
}

int Prenotazione_find(ListHead* list, int codice){ 
	int count = 0; 
	ListItem* ticket_current = list->first; 
	while(ticket_current)
	{
		Prenotazione* p = (Prenotazione*)ticket_current; 
		if(p->codice == codice)
		{
			ListItem* proximo = ticket_current->next;  
			List_detach(list, (ListItem*)p);
			free(p);
			ticket_current = proximo;
			count++;
		}
		else ticket_current = ticket_current->next;
	} 
	return count;
}

int isPostoInLista(Posto p, Posto* lista, int size)
{
	int i;
	for (i=0; i<size; i++)
	{
		if ((lista[i].fila == p.fila) && (lista[i].numero == p.numero))
			return 1;
	}
	return 0;
}

int isPostoValido(Posto* p)
{
	if (isNumeroValido(p->numero) && isFilaValida(&p->fila))
		return 1;
	else
		return 0;
}
	

int isNumeroValido(int numero)
{
	if (numero>=1 && numero<=10)
		return 1;
	else
		return 0;
}

int isFilaValida(char *fila)
{
	if (*fila>='A' && *fila<='I')
		return 1;
	if (*fila>='a' && *fila<='i')
	{
		*fila=*fila-32;
		return 1;
	}
	return 0;
}
	


int Prenotazione_occupata(ListHead* head, int number, char fila){
	ListItem* aux = head->first; 
	while(aux){
		Prenotazione* elem = (Prenotazione*)aux; 
		if((elem->posto.numero == number) && (elem->posto.fila == fila)) 
			return 1; 
		aux= aux-> next; 
	}
		return 0; 
	
}

void Posto_print(Posto* posto, int size){
	int i; 
	for (i= 0; i<size; i++){
		printf("[");
		printf("fila:%c, numero:%d", posto[i].fila, posto[i].numero);
		printf("]\n");
	}
}



int Indice_valido(int numero){
		if(numero >= 0 && numero < 3) return 1; 
		return 0; 
}


