#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <netdb.h>						//gethostbyname, ecc
#include <stdio.h>						//input-output printf
#include <string.h>						//operazioni sulle stringhe (strcmp)
#include <stdlib.h>						//exit
#include <unistd.h>						
#include <assert.h>
#include <signal.h>
#include "struttura.h"

#define Errore_(x){puts(x); exit(1);}
#define PORTA 22501
#define NUM 40


//Variabili Globali
char *indirizzo; //indirizzo server

void analisiArgomenti(int argc, char *argv[]);
void gestionePipe(int num_signal);

int main(int argc, char*argv[]){
	
	int ds_sock; 				//descrittore della socket
	struct sockaddr_in myaddr; 	// indirizzo del server
	struct hostent *hp;	
	
	int codice; 
	int risposta; 
	char buffer[DIM];			//buffer per lettura da tastiera
	Posto new; 					//posto per ciclo
	Posto* lista; 
	int opzione; 				//opzione menu
	int indice; 				//opzione film 	
	int disponibili;			//posti disponibili film
	int numeroPosti;  			//posti prenotati
	int disdetta; 
	int num_film;
	int num_bytes;
	//START
	analisiArgomenti(argc, argv);
	    
	    
	if (signal(SIGPIPE, gestionePipe) == SIG_ERR) {
        perror("signal");
        exit(1);
    }
	// CREAZIONE DELLA SOCKET
	ds_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(ds_sock < 0) Errore_("Errore nel creare la socket..");

	
	myaddr.sin_family = AF_INET;
	myaddr.sin_port =PORTA;
	if ( inet_aton(indirizzo, &myaddr.sin_addr) <= 0 ) { // IP
		printf("client: indirizzo IP non valido.\n client: risoluzione nome...");

		if ((hp=gethostbyname(indirizzo)) == NULL){
			printf("fallita.\n");
  			exit(1);
		}
		printf("riuscita.\n");
		myaddr.sin_addr = *((struct in_addr *)hp->h_addr);
	}
	
	printf("indirizzo server: %s\n", inet_ntoa(myaddr.sin_addr));

	if(connect(ds_sock, (struct sockaddr*)&myaddr, sizeof(myaddr)) < 0) Errore_("Errore di connessione..");
	
	
	
	printf("\nBenvenuto\n");
	while(1)
	{
		printf("--------------------------");
		printf("\nSeleziona l'operazione\n");
		printf("1 - Visualizza elenco di film \n");
		printf("2 - Prenota posti in sala\n");
		printf("3 - Disdici una prenotazione conoscendo il codice\n");
		printf("4 - Esci dal programma\n");
		printf("--------------------------\n");
		printf("Inserisci il numero corrispondente all'operazione scelta:  ");
		
		//AZZERAMENTO VARIABILI
		memset(buffer, 0, sizeof(buffer));
		memset(&new, 0, sizeof(Posto));			
		numeroPosti = 0; 
		num_film = 0; 
		num_bytes = 0; 	
		lista=NULL;
		
		//LETTURA OPZIONE
		fgets(buffer, DIM, stdin);		// inserisco la stringa digitata nel buffer 
		buffer[strlen(buffer)-1]= 0; 	// terminazione stringa con 0
		
		opzione = atoi(buffer); 
		
		switch(opzione)	
		{
			case GET_FILM:
					writeIntero(ds_sock, &opzione);
					
					//readIntero(ds_sock, &num_film);
					for(int i = 0; i<NUMSALE; i++){
						memset(buffer, 0, sizeof(buffer));
						readIntero(ds_sock,&num_bytes);
						read(ds_sock, buffer, num_bytes);
						printf("film[%d]:%s\n", i, buffer);
						}

				break;
			
			case POST_BOOKING: 
					writeIntero(ds_sock, &opzione);
					do{
						printf("Inserisci numero del film:\n");
						memset(buffer, 0, sizeof(buffer));
						fgets(buffer, DIM, stdin); 
						buffer[strlen(buffer)-1] = 0; 
						indice = atoi(buffer);
					}while(!Indice_valido(indice));	
					
					writeIntero(ds_sock, &indice); // invio l'indice del film al server
					readIntero(ds_sock, &disponibili); 
					
					printf("inserisci posto <FILA><NUMERO> \t "); 
					while( disponibili > 0)
					{
						
						memset(buffer, 0, sizeof(buffer));
						fgets(buffer, DIM, stdin); 
						buffer[strlen(buffer)-1] = 0; 
						
						if((strcmp(buffer, "prenota") == 0) || strcmp(buffer, "PRENOTA") == 0)
						{
							if(lista != NULL) break;
							else
							{
								printf("NON HAI PRENOTATO ALCUN POSTO..");
								continue; 						
							}
						}
												
						new.fila = buffer[0];
						new.numero = atoi(buffer+1);
						if (!isPostoValido(&new)) //TESTA VALIDITA' POSTO
						{
							printf("\n Posto non riconosciuto, inserire un altro posto o digitare PRENOTA per inviare\n");
							continue;
						}
						
						if (isPostoInLista(new, lista, numeroPosti)) //TEST DOPPIONE
						{
							printf("\nHai già selezionato questo posto, inserire un altro posto o digitare PRENOTA per inviare\n");
							continue;
						}
							numeroPosti++;
							lista = (Posto*)realloc(lista, (numeroPosti*sizeof(Posto)));
							lista[numeroPosti-1]=new;
							
						disponibili--;
						printf("%d ancora posti disponibili\n", disponibili);
						printf("inserisci<fila><numero> oppure prenota \t"); 
						
					}
					
					
					if(disponibili == 0){
						printf("\n posti terminati per il film %d\n", indice); 
					}
					if(lista == NULL){
						 printf("SCEGLI UN ALTRO FILM, MAGARI TROVERAI POSTI A DISPOSIZIONE....\n");
						 break;
					}
					printf("waiting server\n");
					
					//invio lista al server
					writeIntero(ds_sock, &numeroPosti);
					writeListaPosti(ds_sock, lista, numeroPosti); 
					free(lista); 
					readIntero(ds_sock, &risposta);
					switch(risposta)
					{
						case BOOKING_OK:
							readIntero(ds_sock, &codice);
							readListaPosti(ds_sock, lista, numeroPosti);
							printf("*************************\n");
							printf("PRENOTAZIONE EFFETTUATA CON SUCCESSO\n");
							printf("CODICE PRENOTAZIONE %d:\n", codice);
							printf("RIEPILOGO POSTI PRENOTATI\n");
							Posto_print(lista, numeroPosti);
							printf("*********************\n\n");
							break;
						case BOOKING_FAULT: 
							printf("PRENOTAZIONE FALLITA------------------> \n POSTI PRENOTATI SONO OCCUPATI\n");
							break;
						default: 
							printf("ERRORE DURANTE LA PRENOTAZIONE....\n");
							break;
					}
				break;
				
				case DESTROY_BOOKING: 
					writeIntero(ds_sock, &opzione);	
					printf("INSERISCI CODICE PRENOTAZIONE PER DISDIRE\n");
					memset(buffer, 0, sizeof(buffer));
					fgets(buffer, DIM, stdin); 
					buffer[strlen(buffer)-1] = 0;
					disdetta = atoi(buffer);
					
					writeIntero(ds_sock, &disdetta); 
					readIntero(ds_sock, &opzione); 
					switch(opzione){
						case DESTROY_COMPLET: 
							printf("DISTETTA EFFETTUATA CON SUCCESSO"); 
							break;
						case DESTROY_FAULT: 
							printf("BIGLIETTO NON TROVATO!!!!!");
							break;
						default: 
							printf("ERRORE DURANTE LA DISDETTA..");
							break;
						}
					
					
					break;
				
			
				case CLIENT_EXIT: 
					writeIntero(ds_sock, &opzione);  //INVIO SELEZIONE
					close(ds_sock);
				return 0;
				
				default:
				printf("Opzione non riconosciuta. Riprovare...");
				break;
			}

		}
		
		
		
}
void gestionePipe(int num_signal){
	printf("Comunicazione interrotta con il server\n"); 
	exit(1);
}

void analisiArgomenti(int argc, char *argv[])
{
	int n=1;
	while (n<argc)
	{
		if (!strncmp(argv[n], "-a", 2))
			indirizzo = argv[n+1];
		else
			if (!strncmp(argv[n], "-h", 2))
			{
				printf("Sintassi:\n\n");
				printf("    client -a (indirizzo) [-h].\n\n");
				exit(0);
			}
		n++;
	}
	if (argc!=3)
	{
		printf("Sintassi:\n\n");
		printf("    client -a (indirizzo) [-h].\n\n");
	    exit(1);
	}
}
