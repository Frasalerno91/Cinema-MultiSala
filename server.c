#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>

#include "struttura.h"
#define Errore_(x){puts(x);exit(1);}
#define PORTA 22501
#define MAX_CONNECT 5

//variabili globali
volatile int num_threads_alive = 0; 
Cinema cinema;

typedef struct Pthread_arg_t{
	int new_client_socket;
	struct sockaddr_in client_address;
	int id;
}Pthread_arg_t;




// prototipi
void* gestione_Cliente(void* args);
void signal_handler(int signal_number);


int main(int argc, char* argv[])
{
	int descr_sock;
	int client_sock;
	struct sockaddr_in server_address; 
	socklen_t size_address; 
	pthread_attr_t pthread_attr;
	pthread_t pthread;
	Pthread_arg_t* pthread_arg;
	
	// INIZIALIZZO LO CINEMA
	char* room_names[NUMSALE];
	room_names[0] = "Game Nigth";
	room_names[1] = "Sistemi Operativi";
	room_names[2] = "Avengers";

	printf("CREATED THE CINEMA\n\n");
	Cinema_init(&cinema, NUMSALE, room_names);
	Cinema_print(&cinema);
	printf("\n");
	
	//Socket--> Creazione TCP socket
	descr_sock = socket(AF_INET, SOCK_STREAM, 0); 
	if(descr_sock < 0 ) Errore_("Errore nel creare una socket.. ");
	
	//inizializzo IPv4 address
	memset(&server_address, 0, sizeof(server_address)); //Settaggio tutti i byte a zero
	server_address.sin_family= AF_INET; // dominio di indirizzi IP:
	server_address.sin_port = PORTA;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY); // permetto che qualsiasi host si connetti alla socket
	
	// Bind	--> assegnazione indirizzo in modo da poter essere contattati e ricevere dati
	if(bind(descr_sock, (struct sockaddr*) &server_address, sizeof(server_address)) < 0 ) Errore_("Errore nella bind.."); // se esiste un socket attivo con stesso numero di porta
	
	// Listen --> numero di connessioni entranti
	if(listen(descr_sock, MAX_CONNECT) < 0) Errore_("Errore nella listen..");

	// Assign signal handlers to signals
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
        perror("signal");
        exit(1);
    }
    if (signal(SIGTERM, signal_handler) == SIG_ERR) {
        perror("signal");
        exit(1);
    }
    if (signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("signal");
        exit(1);
	}
	// initialize pthread attribute
	 if (pthread_attr_init(&pthread_attr) != 0) {
        perror("pthread_attr_init");
        exit(1);
    }
    if (pthread_attr_setdetachstate(&pthread_attr, PTHREAD_CREATE_DETACHED) != 0) {
        perror("pthread_attr_setdetachstate");
        exit(1);
    }

	printf("Server in attesa sulla porta: %d \n", PORTA);
	printf("Per chiudere correttamente il server Control+C\n");

	

	// ciclo per accettare connessioni
  while(1)	
		{
		
			pthread_arg = (Pthread_arg_t*)malloc(sizeof *pthread_arg);
			if (!pthread_arg) {
				perror("malloc");
				continue;
			}
			size_address = sizeof pthread_arg->client_address;
			client_sock = accept(descr_sock, ((struct sockaddr*)&pthread_arg->client_address), &size_address);
			if(client_sock == -1){
				printf("Errore nell'accettare connessioni.."); 
				free(pthread_arg);
				continue;
			}
			pthread_arg->new_client_socket = client_sock;
			pthread_arg->id = num_threads_alive+1;
			if (pthread_create(&pthread, &pthread_attr, gestione_Cliente, (void *)pthread_arg) != 0) {
				perror("pthread_create");
				free(pthread_arg);
            continue;
        }
	}

	Cinema_destroy(&cinema);
	return 0;
}

void* gestione_Cliente(void* args){
	Pthread_arg_t* pthread_arg = (Pthread_arg_t*)args;
	int sock = pthread_arg->new_client_socket;	
	struct sockaddr_in ip_client = pthread_arg-> client_address;
	int id = pthread_arg->id; 
	char* new_ip = inet_ntoa(ip_client.sin_addr); 
	free(args);
	
	int opzione;		 // variabile in cui memorizzo la scelta del cliente
	int numeroPosti;	// numero posti prenotati 	 
	int indFilm;		// indice del film scelto
	Posto* listaPosti;	
	int codice; 
	int i;
	int responso;
	int disdetta;
	num_threads_alive++;
	ssize_t nbytes;
 	
 	while(1)
    {
		
      // aggiornamento variabili
      listaPosti = NULL;
      numeroPosti = 0;
      indFilm = -1;
      responso = -1;
      codice = -1;
      disdetta = -1;
	  nbytes = 0;
	  
      printf("Numero Thread attivi nel server:%d\n", num_threads_alive);
      printf("[Thread %d]Attendo scelta opzione dal cliente:%s\n",id, new_ip);
      
      readIntero(sock, &opzione); //RICEZIONE SELEZIONE
      switch (opzione) {
	  case GET_FILM: 
			{
				//	writeIntero(sock, &cinema.num_sale);
					for(int i = 0; i<cinema.num_sale; ++i){
						char* name = cinema.sala_array[i].film;
						nbytes = strlen(name);
						writeIntero(sock, (int*)&nbytes); 
						write(sock, name, nbytes);
					}
				break;
			}

      case POST_BOOKING:
				{
					printf("[CLIENT %d]Ha scelto POST_BOOKING\n",id);
					readIntero(sock, &indFilm);				//ricevo indice del film selezionato
					
					Sala* current_sala = &cinema.sala_array[indFilm];

					printf("[CLIENT %d]FILM  selezionato:%s\n",id, current_sala->film);
					writeIntero(sock, &current_sala->max_posti);
					if(&current_sala->max_posti == 0)
					{
						printf("POSTI ESAURITI PER IL FILM SCELTO\n");
						break;
					}
					// ricevo lista posti dal client
          
					readIntero(sock, &numeroPosti);
					listaPosti = (Posto*)malloc(numeroPosti*sizeof(Posto));
					readListaPosti(sock, listaPosti, numeroPosti);		//RICEZIONE LISTA POSTI
					printf("Numero posti ricevuti da [CLIENT %d]: %d\n", id, numeroPosti);
					//Posto_print(listaPosti, numeroPosti);
					
					// CONTROLLO SE QUALCHE POSTO SI TROVA NELLA LISTA
					int controllo = 0;
					
					ListHead* test_prenotazione = &current_sala->lista_prenotazioni;
					for(i = 0; i<numeroPosti; i++){
						if(Prenotazione_occupata(test_prenotazione,listaPosti[i].numero, listaPosti[i].fila)){
							controllo = 1;
							break;
						}
					}
					
					if(controllo)
					{
						printf("Qualche posto è occupato\n");
					
					}
					else
					{
						codice = (time(NULL) | rand());
						Prenotazione_create(test_prenotazione, listaPosti, numeroPosti, codice);								
						Prenotazione_print(&current_sala->lista_prenotazioni);
						current_sala->max_posti -= numeroPosti;
						current_sala->prenotazioni_attive += numeroPosti;
						printf("Elenco prenotazioni aggiornato\n");
					}
				
					if (controllo)
					{
						responso=888;
						writeIntero(sock, &responso);
					}
					else
					{
						responso=111;
						writeIntero(sock, &responso);
						writeIntero(sock, &codice); //INVIO CODICE DI PRENOTAZIONE
						writeListaPosti(sock, listaPosti, numeroPosti);
					}
					
					printf("CINEMA POST-PRENOTAZIONE\n");
					Cinema_print(&cinema);
					break;
	
			}
			case DESTROY_BOOKING:
			{
				printf("[CLIENT %d]destroy booking\n",id);
				readIntero(sock, &disdetta);
				printf("[CLIENT %d]codice del biglietto da disdire:%d\n",id, disdetta);
				int seat_destroy = 0;
				int result; 
				int numero_eliminati; 
				for(int i=0; i<NUMSALE; ++i){
					Sala* current_sal = &cinema.sala_array[i];  
					numero_eliminati = Prenotazione_find(&current_sal->lista_prenotazioni, disdetta);
					cinema.sala_array[i].max_posti += numero_eliminati;   
					cinema.sala_array[i].prenotazioni_attive -= numero_eliminati;
					seat_destroy += numero_eliminati;
				}
					
				if(seat_destroy == 0) result = 0; 
				else result = 1; 
				
				if(result){
					responso = 222; 
					writeIntero(sock, &responso); 	
				}	
				else {
					responso = 777; 
					writeIntero(sock, &responso); 
				}
				printf("STAMPO LO CINEMA\n");
				Cinema_print(&cinema);
				printf("LO CINEMA E' STATO AGGIORNATO");
				
			break;
			}
    		
			case CLIENT_EXIT:
			{	
				printf("[CLIENT %d]Chiusura del Client %s\n",id, new_ip);
				num_threads_alive--;
				printf("Numero Threads atttivi nel server:%d\n",num_threads_alive);
				close(sock);
				pthread_exit(NULL);
				return NULL;
			}
			default:
				printf("[CLIENT %d]Opzione non riconosciuta da %s. Il client in questione verrà disconnesso\n",id, new_ip);
				num_threads_alive--;
				printf("Numero Threads attivi nel server:%d\n",num_threads_alive);
				close(sock);
				pthread_exit(NULL);
				return NULL;
		}
    }
  
}

void signal_handler(int signal_number) {
	printf("SERVER CHIUSO IN MODO CORRETTO \n");
	Cinema_destroy(&cinema);
	exit(0);
}
